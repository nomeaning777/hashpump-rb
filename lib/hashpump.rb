require 'ext'

# HashPumpを用いてHash Length Extension attackを行うための関数を定義したモジュールです
module HashPump
  # HashPumpを用いてハッシュ拡張攻撃を行います。ハッシュ値の種類はハッシュの長さにより自動的に識別されます。
  # @param  [String]  signature   拡張するハッシュ値(デフォルトでは16進数文字列)
  # @param  [String]  known_data  ハッシュされた文字列のうち既知の後半部分
  # @param  [String]  append_data 追加したい文字列
  # @param  [Integer] key_length  ハッシュされた文字列のうち未知の前半部分の長さ
  # @param  [Boolean] hex_digest  ハッシュ値を16進数文字列として扱うか。デフォルトではtrue。
  # @return [Array] 最初の要素に拡張後のハッシュ値(16進数文字列), 2番目の要素に追加する文字列を持つ配列を返す
  def hashpump(signature, known_data, append_data, key_length, hex_digest = true)
    if hex_digest
      signature = signature.unpack("H*")[0] unless /^[A-Fa-f0-9]*$/ =~ signature
    else
      signature = signature.unpack("H*")[0]
    end
    signature, data = call_hashpump([signature.to_s].pack("H*").unpack("C*"), known_data.to_s.unpack("C*"), append_data.to_s.unpack("C*"), key_length)
    signature = signature.pack("C*").unpack("H*")[0]
    data = data.pack("C*")
    signature = [signature].pack("H*") unless hex_digest
    [signature, data]
  end

  module_function :call_hashpump, :hashpump
end
