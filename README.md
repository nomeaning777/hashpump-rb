# HashPump-rb

A extension library to use HashPump(https://github.com/bwall/HashPump) from Ruby

## Installation

```
$ git submodule init
$ git submodule update
$ rake build
$ rake install
```

## Example

```ruby
require 'digest/md5'
require 'hashpump'

new_signature, data = HashPump::hashpump(Digest::MD5.hexdigest('unknown' + 'hoge'), 'hoge', 'append_data', 7)
p new_signature == Digest::MD5.hexdigest('unknown' + data)
```

## Command Line Tool

```
$ hashpump-rb --signature 900150983cd24fb0d6963f7d28e17f72 --data bc --additional hoge --keylength 1
1e03f6cf2ef653b49b0043b07c5675f8
626380000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001800000000000000686f6765
```
