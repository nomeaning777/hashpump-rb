# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'hashpump/version'

Gem::Specification.new do |spec|
  spec.extensions    = ["ext/extconf.rb"]

  spec.name          = "hashpump-rb"
  spec.version       = HashPump::VERSION
  spec.authors       = ["nomeaning"]
  spec.email         = ["nomeaning@mma.club.uec.ac.jp"]
  spec.summary       = %q{HashPump ruby wrapper}
  spec.description   = %q{HashPump ruby wrapper}
  spec.homepage      = "https://bitbucket.org/nomeaning777/hashpump-rb"
  spec.license       = "MIT-LICENSE"
  submodule_files   = []
  submodule_path    = `git submodule --quiet foreach pwd`.split(/\n/)[0]
  puts submodule_path
  Dir.chdir(submodule_path) do
    submodule_files  = `git ls-files -z`.split("\x0").map{|filename|
      File.join(submodule_path, filename).gsub(File.dirname(__FILE__) + "/" , '')
    }
  end

  spec.files         = `git ls-files -z`.split("\x0") + submodule_files
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})

  spec.require_paths = ["ext", "lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
end
