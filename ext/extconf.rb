require 'mkmf'
$CPP_STANDARD = 'c++11'
have_library("stdc++")
have_library("crypto")
$CPPFLAGS += " -O3 -std=#{$CPP_STANDARD}" 

basenames = %W(hashpump) + (Dir.glob("HashPump/*.cpp") - %W(HashPump/main.cpp HashPump/hashpumpy.cpp)).map{|a|
  a[0..-5] # '.cpp'を取り除く
}
$objs = basenames.map { |b| "#{b}.o" }
$srcs = basenames.map { |b| "#{b}.cpp" }
$cleanobjs = '*.o HashPump/*.o'
create_makefile("hashpump/ext")  
File.open "Makefile", "a" do |f|
  f.write "CLEANOBJS := $(CLEANOBJS) HashPump/*.o HashPump/*.bak"
end
