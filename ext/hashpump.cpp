#include <string>
#include <vector>
#include "HashPump/SHA1.h"
#include "HashPump/MD5ex.h"
#include "HashPump/SHA256.h"
#include "HashPump/SHA512ex.h"
#include "HashPump/MD4ex.h"
#include "HashPump/CRC32ex.h"
#include "ruby.h"

static Extender * GetExtenderForHash(const vector<unsigned char> &sig)
{
  if(sig.size() == 20)
  {
    return new SHA1ex();
  }
  else if(sig.size() == 32)
  {
    return new SHA256ex();
  }
  else if(sig.size() == 16)
  {
    return new MD5ex();
  }
  else if(sig.size() == 64)
  {
    return new SHA512ex();
  }
  else if(sig.size() == 4)
  {
    return new CRC32ex();
  }
  return NULL;
}


static pair<vector<unsigned char>, vector<unsigned char>> hashpump(const vector<unsigned char> &known_signature, 
    const vector<unsigned char> &data, 
    const vector<unsigned char> &append_data, 
    int key_length) {
  auto extender = GetExtenderForHash(known_signature);
  if(extender == NULL) {
    rb_raise(rb_eStandardError, "Unsupported hash size.");
    return make_pair(vector<unsigned char>(), vector<unsigned char>());
  }
  unsigned char* first_signature = new unsigned char[known_signature.size()];
  for(size_t i = 0; i < known_signature.size(); i++) {
    first_signature[i] = known_signature[i];
  }
  unsigned char* result;
  vector<unsigned char> * result_bytes = extender->GenerateStretchedData(data, key_length, first_signature, append_data, &result);
  delete[] first_signature;
  vector<unsigned char> new_signature(known_signature.size());
  for(size_t i = 0; i < known_signature.size(); i++) {
    new_signature[i] = result[i];
  }
  vector<unsigned char> result_bytes_cp = *result_bytes;
  delete[] result;
  delete result_bytes;
  return make_pair(new_signature, result_bytes_cp);
}

static vector<unsigned char> rbarray_to_vector(VALUE array) {
  vector<unsigned char> result(RARRAY_LEN(array));
  for(size_t i = 0; i < result.size(); i++) {
    Check_Type(rb_ary_entry(array, i), T_FIXNUM);
    result[i] = FIX2INT(rb_ary_entry(array, i));
  }
  return result;
}
/*
 * @overload call_hashpump(signature, data, append, length)
 * 実際にHashPumpを呼び出し、ハッシュ拡張を行う関数です。
 */
static VALUE call_hashpump(VALUE self, VALUE signature, VALUE data, VALUE append, VALUE length) {
  Check_Type(signature, T_ARRAY);
  Check_Type(data, T_ARRAY);
  Check_Type(append, T_ARRAY);
  Check_Type(length, T_FIXNUM);
  auto result = hashpump(
      rbarray_to_vector(signature),
      rbarray_to_vector(data),
      rbarray_to_vector(append),
      FIX2INT(length)
  );
  VALUE res = rb_ary_new();
  VALUE res_sig = rb_ary_new();
  VALUE res_data = rb_ary_new();
  for(auto c: result.first) {
    rb_ary_push(res_sig, INT2FIX(c));
  }
  for(auto c: result.second) {
    rb_ary_push(res_data, INT2FIX(c));
  }
  rb_ary_push(res, res_sig);
  rb_ary_push(res, res_data);
  return res;
}

extern "C" {
  void Init_ext()
  {
    VALUE module = rb_define_module("HashPump");
    rb_define_module_function(module, "call_hashpump", (VALUE(*)(...))call_hashpump, 4);
  }
}
